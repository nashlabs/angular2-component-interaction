import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NameParentComponent }         from './name-parent.component';
import { NameChildComponent }         from './name-child.component';

import { VoteTakerComponent }         from './vote-taker.component';
import { VoterComponent }         from './voter.component';

import { VersionParentComponent }         from './version-parent.component';
import { VersionChildComponent }         from './version-child.component';

import { CountdownLocalVarParentComponent }         from './countdown-localVar-parent.component';
import { CountdownViewChildParentComponent }         from './countdown-viewChild-parent.component';
import { CountdownTimerComponent }         from './countdown-timer.component';

import { MissionControlComponent }         from './mission-control.component';
import { AstronautComponent }         from './astronaut.component';

@NgModule({
  imports: [
    BrowserModule
  ],
  declarations: [
    NameParentComponent, NameChildComponent,
    VoteTakerComponent, VoterComponent,
    VersionParentComponent, VersionChildComponent,
    CountdownLocalVarParentComponent, CountdownViewChildParentComponent, CountdownTimerComponent,
    MissionControlComponent, AstronautComponent
  ],
  bootstrap: [
    NameParentComponent,
    VoteTakerComponent,
    VersionParentComponent,
    CountdownLocalVarParentComponent,
    CountdownViewChildParentComponent,
    MissionControlComponent ]
})
export class AppModule { }
