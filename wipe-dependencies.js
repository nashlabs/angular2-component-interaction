const fs = require('fs'),
      cpFile = require('cp-file'),
      wipeDependencies = function() {
        cpFile('package.json', 'package.json.backup').then(() => {
          console.info('package.json backup file created');
          let file  = fs.readFileSync('package.json'),
            content = JSON.parse(file);
          for (let devDep in content.devDependencies) {
            content.devDependencies[devDep] = '*';
          }
          for (let dep in content.dependencies) {
            content.dependencies[dep] = '*';
          }
          fs.writeFileSync('package.json', JSON.stringify(content,null,2));
        })
        .catch(() => {
          throw new Error("Wipe dependencies: Couldn't backup package.json file");
        });
      };



if (require.main === module) {
  wipeDependencies();
} else {
  module.exports = wipeDependencies;
}
